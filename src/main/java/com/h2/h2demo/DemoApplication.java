package com.h2.h2demo;

import com.h2.h2demo.domain.Subscriber;
import com.h2.h2demo.repository.SubscriberRepository;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(SubscriberRepository repository) {
		return args -> {
			repository.save(new Subscriber("Dan", "Cummings", "timesuck@email.com"));
		};
	}

}
