package com.h2.h2demo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Subscriber {

    @Id
    @GeneratedValue
    private Long id;
    private String first;
    private String last;
    private String email;

    public Subscriber() {
        super();
    }

    public Subscriber(String first, String last, String email) {
        this.first = first;
        this.last = last;
        this.email = email;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst() {
        return this.first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return this.last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "{" + " first='" + getFirst() + "'" + ", last='" + getLast() + "'" + "}";
    }

}