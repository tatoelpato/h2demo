package com.h2.h2demo.repository;

import com.h2.h2demo.domain.Subscriber;

import org.springframework.data.repository.CrudRepository;

public interface SubscriberRepository extends CrudRepository<Subscriber, Long> {

}